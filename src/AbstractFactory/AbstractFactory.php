<?php

namespace AbstractFactory;

/**
 * Class AbstractFactory| my OOP patterns design
 * @package AbstractFactory
 * @author robotomzie@gmail.com
 * @version 1.0.1
 * @usage Ufactory extends AbstractFactory
 */
abstract class AbstractFactory
{
    abstract public function createGenerator();
    abstract public function createArray();

}