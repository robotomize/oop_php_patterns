<?php

namespace AbstractFactory;

/**
 * Class PictureArrayGenerator
 * @package AbstractFactory
 * @author robotomzie@gmail.com
 */
class PictureArrayGenerator
{

    function __construct()
    {
    }

    function __toString()
    {
        return 'Picture array genrated';
    }

}