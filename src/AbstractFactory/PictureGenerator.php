<?php

namespace AbstractFactory;

/**
 * Class PictureGenerator
 * @package AbstractFactory
 * @author robotomzie@gmail.com
 * @usage
 */
class PictureGenerator
{

    function __construct()
    {
    }

    function __toString()
    {
        return 'Picture generated';
    }


}