<?php

namespace AbstractFactory;

/**
 * Class TextArrayGenerator
 * @package AbstractFactory
 * @author robotomzie@gmail.com
 */
class TextArrayGenerator
{

    function __construct()
    {
    }

    function __toString()
    {
        return 'Text array generator';
    }

}