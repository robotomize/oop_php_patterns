<?php

namespace AbstractFactory;

/**
 * Class TextGenerator
 * @package AbstractFactory
 * @author robotomize@gmail.com
 */
class TextGenerator
{

    function __construct()
    {

    }

    /**
     * @return string
     */
    function __toString()
    {
        return 'Text generated';
    }


}