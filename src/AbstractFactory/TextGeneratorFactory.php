<?php

namespace AbstractFactory;

include 'AbstractFactory.php';
include 'TextGenerator.php';
include 'TextArrayGenerator.php';

/**
 * Class TextGeneratorFactory
 * @package AbstractFactory
 * @author robotomzie@gmail.com
 * @usage create text objects
 */
class TextGeneratorFactory extends AbstractFactory
{
    public function createGenerator()
    {
        return new TextGenerator();
    }

    public function createArray()
    {
        return new TextArrayGenerator();
    }
}