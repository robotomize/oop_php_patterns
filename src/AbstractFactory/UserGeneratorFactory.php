<?php


namespace AbstractFactory;

include 'PictureGenerator.php';
include 'PictureArrayGenerator.php';

/**
 * Class UserGeneratorFactory
 * @package AbstractFactory
 * @author robotomize@gmail.com
 * @usage create picture object
 */
class UserGeneratorFactory
{
    public function createGenerator()
    {
        return new PictureGenerator();
    }

    public function createArray()
    {
        return new PictureArrayGenerator();
    }
}