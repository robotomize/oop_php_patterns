<?php

namespace ProductFactory;

include 'ProductFactory.php';

/**
 * Class ProductController
 * @package ProductFactory
 * @author robotomzie@gmail.com
 */
class ProductController
{
    /**
     * @throws \Exception
     */
    public function makeObj()
    {
        ProductFactory::build('UserGeneratorFactory');
    }

}