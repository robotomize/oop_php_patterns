<?php

namespace ProductFactory;

include '../AbstractFactory/UserGeneratorFactory.php';
include '../AbstractFactory/TextGeneratorFactory.php';

use AbstractFactory\UserGeneratorFactory;
use AbstractFactory\TextGeneratorFactory;

/**
 * Class ProductFactory
 * @package ProductFactory
 * @author robotomize@gmail.com
 */
class ProductFactory
{
    /**
     * @param $productType
     * @return mixed
     * @throws \Exception
     */
    public static function build($productType)
    {
        if (class_exists($productType)) {
            return new $productType();
        } else {
            throw new \Exception('Class load error');
        }
    }
}