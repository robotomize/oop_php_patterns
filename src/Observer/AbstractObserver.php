<?php

namespace Observer;

/**
 * Class AbstractObserver
 * @package Observer
 *
 */
abstract class AbstractObserver
{
    /**
     * Observer update method
     * @param $subject_in
     * @return mixed
     */
    abstract function update($subject_in);
}