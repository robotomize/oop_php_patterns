<?php

namespace Observer;

/**
 * Class AbstractSubject
 * @package Observer
 */
abstract class AbstractSubject
{
    /**
     * Subscribe on
     * @param $observer_in
     * @return mixed
     */
    abstract function attach($observer_in);

    /**
     * Subscribe off
     * @param $observer_in
     * @return mixed
     */
    abstract function detach($observer_in);

    /**
     * Event notify
     * @return mixed
     */
    abstract function notify();
}