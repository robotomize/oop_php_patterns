<?php

namespace Observer;

require_once 'AbstractSubject.php';

/**
 * Class Observer|
 * @package Observer
 */
class Observer extends AbstractObserver
{
    /**
     * @construct
     */
    public function __construct() {

    }

    /**
     * Update method
     * @param $subject
     */
    public function update($subject) {
        print ('*IN PATTERN OBSERVER - NEW PATTERN GOSSIP ALERT*')  . PHP_EOL;
        print ('New Favorite Pattern' . $subject->getFavorites())   . PHP_EOL;
        print ('*IN PATTERN OBSERVER - PATTERN GOSSIP ALERT OVER*') . PHP_EOL;
    }
}