<?php

namespace Observer;

require_once 'AbstractObserver.php';
require_once 'AbstractSubject.php';
require_once 'Observer.php';

/**
 * Class Subject
 * @package Observer
 */
class Subject extends AbstractSubject
{
    /**
     * @var null
     */
    private $favoritePatterns = NULL;

    /**
     * @var array
     */
    private $observers = array();

    /**
     *
     */
    function __construct() {

    }

    /**
     * event subscribe on
     * @param $observer_in
     */
    function attach($observer_in) {
        $this->observers[] = $observer_in;
    }

    /**
     * Event subscribe off
     * @param $observer_in
     */
    function detach($observer_in) {
        foreach($this->observers as $okey => $oval) {
            if ($oval == $observer_in) {
                unset($this->observers[$okey]);
            }
        }
    }

    /**
     * Event notify
     */
    function notify() {
        foreach($this->observers as $obs) {
            $obs->update($this);
        }
    }

    /**
     * @param $newFavorites
     */
    function updateFavorites($newFavorites) {
        $this->favorites = $newFavorites;
        $this->notify();
    }

    /**
     * @return mixed
     */
    function getFavorites() {
        return $this->favorites;
    }
}