<?php

namespace Observer;

require_once 'AbstractObserver.php';
require_once 'AbstractSubject.php';
require_once 'Observer.php';
require_once 'Subject.php';

$patternGossiper = new Subject();
$patternGossipFan = new Observer();

$patternGossiper->attach($patternGossipFan);
$patternGossiper->updateFavorites('abstract factory, decorator, visitor');

$patternGossiper->updateFavorites('abstract factory, observer, decorator');
$patternGossiper->detach($patternGossipFan);
$patternGossiper->updateFavorites('abstract factory, observer, paisley');